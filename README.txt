PREFACE
-------
This module provides an easy way to extend the lazyloading of
images with a Pinterest effect. By displaying the modal color
of the images as placeholder, while the images are being
loaded, the users gets more and better feedback of how the
page will look.

CONTENTS
--------
1. Installation
2. Configuration
3. Thanks

1. Installation
--------------------------------------------------------------------------------
Before installing PAAS LazyColor, you need the LazyLoader module. 
If not already installed, download the latest stable version at:
https://drupal.org/project/lazyloader
Please follow the readme file provided by LazyLoader on how to install.

Copy PAAS-LazyColor into your modules directory (i.e. sites/all/modules)
and enable PAAS-LazyColor (on admin/build/modules).

2. Configuration
--------------------------------------------------------------------------------
Currently, there is no configuration for this module, "It Just Works". 
If you have a big page with many images and / or a slower page, 
you can see the colorful placeholders of the images that are being loaded.
An example can be seen on http://go.brlnd.nl/PAAS-LazyColor-example

3. Thanks
--------------------------------------------------------------------------------
thanks for Pinterest for inspiring us. Thanks ProjectPAAS.com for donating this
module, faster websites, period!
